import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

import { SERVICE_INSTANCE_MOCK, MILLICORES_MOCK } from './chart.mock';
import { IChartData } from './chart.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options;
  
  readonly serviceInstanceKey: string = 'Service Instance Count';
  readonly millicoresKey: string = 'CPU Usage Millicores';

  yA: number[][] = this.normalizeData(SERVICE_INSTANCE_MOCK, this.serviceInstanceKey);
  yB: number[][] = this.normalizeData(MILLICORES_MOCK, this.millicoresKey);

  ngOnInit(): void {
    this.chartOptions = {
      chart: {
        zoomType: 'xy',
        backgroundColor: '#f7f7f7',
        plotBackgroundColor: '#fff',
        marginTop: 35,
        plotBorderWidth: 1
      },
      legend: {
        enabled: false
      },
      title: {
        text: null
      },
      xAxis: [{
        gridLineWidth: 1,
        gridLineColor: '#f9f9f9',
        type: 'datetime',
        dateTimeLabelFormats: {
          minute: '%I:%M',
          hour: '%I %p',
        },
        labels: { 
          formatter: function() {
            return Highcharts.dateFormat(new Date(this.value).getMinutes() > 0 ? '%I:%M' : '%I %p', this.value); 
          } 
        },
        tickInterval: 15 * 60 * 1000
      }],
      plotOptions: {
        series: {   
          marker: {
            enabled: false
          },
          pointStart: this.yA[0][0],
          pointInterval: 15 * 60 * 1000 // 15 min
        },
      },
      yAxis: [{
        tickAmount: 8,
        tickInterval: 1,
        labels: {
          format: '{value}'
        },
        title: {
          text: 'Instance Count',
          style: {
            color: '#999'
          },
          align: 'high',
          offset: -88,
          rotation: 0,
          y: -10
        }
      }, {
        tickAmount: 8,
        labels: {
          formatter: function() {
            return this.value >= 1000 ? `${(this.value/1000).toFixed(1)}k` : String(this.value);
          }
        },
        title: {
          text: 'millicores',
          style: {
            color: '#999'
          },
          align: 'high',
          offset: -58,
          rotation: 0,
          y: -10
        },
        gridLineColor: '#f9f9f9',
        opposite: true
      }],
      tooltip: {
        shared: true
      },
      series: [{
        name: this.serviceInstanceKey,
        pointWidth: 9,
        pointPlacement: 'between',
        type: 'column',
        data: this.yA,
        color: '#8ad1ee',
      }, {
        yAxis: 1,
        name: this.millicoresKey,
        type: 'spline',
        data: this.yB,
        color: '#2965a6'
      }]
    };
  }

  onChange(event, key: string): void {
    const legend = Highcharts.charts[0].series.find(item => item.name === key);
    if (event.target.checked) {
      legend.show();
    } else {
      legend.hide();
    }
  }

  private refresh(): void {
    this.yA = [];
    this.yB = [];
    let time = new Date(Date.now() + Math.round(Math.random()*5*24*60*60) * 1000);
    time.setSeconds(0);
    time.setMilliseconds(0);
    let timestamp = time.getTime();
    for (let i = 0; i < 120; i++) {
      const copy = +(Math.random() * 1.8).toFixed(0)
      const instanceVal = copy && i > 0 ? this.yA[i - 1][1] : Math.floor(Math.random() * 12);
      const coof = Math.floor(Math.random() * 250) + 150;
      const milliVal = instanceVal * coof;

      this.yA.push([ timestamp, instanceVal ]);
      this.yB.push([ timestamp, milliVal ]);
      timestamp += 30000;
    }
    this.ngOnInit();
  }

  private normalizeData(data: IChartData[], key: string): number[][] {
    return data.map((item: IChartData)  => ([
      Date.parse(item.time),
      item[key] || 0
    ]));
  }
}