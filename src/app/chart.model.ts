export interface IChartData {
  time: string,
  'Service Instance Count'?: number;
  'CPU Usage Millicores'?: number;
};